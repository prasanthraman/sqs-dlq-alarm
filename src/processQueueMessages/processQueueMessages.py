import random
class MyException(Exception):
    pass
def lambda_handler(event,context):
    print(event)

    batch_item_failures = []
    sqs_batch_response = {}
    
    for record in event["Records"]:
        try:
            # process message
            x = random.randint(1,10)
            if x >=5:
                raise MyException("Need to handle cases above 5")
            else:
                pass

        except Exception as e:
            batch_item_failures.append({"itemIdentifier": record['messageId']})
    
    sqs_batch_response["batchItemFailures"] = batch_item_failures
    return sqs_batch_response
    
